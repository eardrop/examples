// My tiny agular component to add some "go back" behavior to pages..

(function () {
    var m = angular.module('utilities');

    m.component('backButton', {
        templateUrl: 'static/utilities/back-button.html',
        bindings: {
            action: '<',
            disabled: '<'
        },

        controller: ['$scope', '$location', '$log', function($scope, $location, $log) {
            function goBack() {
                window.history.back();
            }

            var ctrl = this;
            ctrl.scope = $scope;

            function checkAction() {
                if (ctrl.action) {
                    if (typeof ctrl.action === 'function') {
                        return true;
                    }
                    $log.debug('backButtonComponent: goBack action is not valid', action.toString());
                }
                return false;
            }

            $scope.goBack = function() {
                if (checkAction()) {
                    $log.debug('backButtonComponent: goBack with action', ctrl.action.toString());
                    return ctrl.action();
                }
                goBack();
            };

            ctrl.$onInit = function() {
                if (document.referrer !== "" && !~document.referrer.indexOf($location.$$host) && !checkAction()) {
                    ctrl.scope.hide = true;
                }
            };
        }]
    });
})();

// Use case
// Ussually used like this
$scope.someFunction = function () {
    // for example: get some data  to update the view
}

$scope.someFlag = function () {
    // for example: return loading state
    return loadingState
}

<back-button action="someFunction" disabled="someFlag"></back-button>
