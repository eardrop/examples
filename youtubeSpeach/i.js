const audioThreshold = 60;
var active = false;
var command = '';
var text = '';
function stopActive() {
  active = false;
}

const commands = {
  "fullscreen": ["full screen", "full stream", "fulcrum", "will scream", "view screen", "Christian", "who's", "Brooklyn"],
  "volumeup": ["volume up"],
  "volumedown": ["volume down", "Orlando"],
  "mute": ["mute", "mood"],
  // "search": ["search"],
  "play": ["play", "put", "plan", "plant"],
  "stop": ["stop"],
  "default": ["default"],
  "select": ["select", "silly", "celiac", "Philip"],
  "previous": ["previous", "prev", "pre", "Prive", "pretty", "trivia"],
  "next": ["next"],
  "back": ["back", "but", "bag"]
}  

function checkCommand(text) {
  for (comI in commands) {
    for (i in commands[comI]) {
      if (text === commands[comI][i]) {
        return comI
      }
    }
  }
}

function SpeechClass() {
  if ('webkitSpeechRecognition' in window) {
    this.recognition = new webkitSpeechRecognition();
    
    this.recognition.continuous = false; // stop automatically
    this.recognition.interimResults = true;
      
    var that = this;  
      
    this.startCapture = function() {
      this.recognition.start();
      active = true;
    }
    
    this.stopCapture = function() {
      this.recognition.stop();
    }
    
    this.thresholdExceeded = function() {
      if (!active) {
        command = '';
        this.startCapture();
      }
    }
    
    this.recognition.onresult = function(event) {
      text = event.results[0][0].transcript
      console.log(text)
      command = checkCommand(text)
      if (command) {
        console.log('!!!!', command)
        that.stopCapture()
      }
    }
    
    this.recognition.onerror = function(event) {
      stopActive()
      console.log(event.error);
    }
    
    this.recognition.onend = function(event) {
      console.log('end', text)
      if (text.indexOf("search") > -1) {
        sendMessage("search", text.replace("search", ""))
      } else if (command) {
        sendMessage(command)
      }        
  
      stopActive()      
    };
    
    this.recognition.onnomatch = stopActive;
    // onsoundend
    // onspeechend

        
    console.log("webkitSpeechRecognition is available.");
  } else {
    console.log("webkitSpeechRecognition is not available.");
  }
}
const Speech = new SpeechClass();
navigator.mediaDevices.getUserMedia({audio: true}).then( 
  function(stream) {
    start_microphone(stream);
  },
  function(e) {
    alert('Error capturing audio.');
  }
)

function sendMessage(command, message) {
  chrome.runtime.sendMessage({
    command, 
    value: message
  })
}

var audioContext = new AudioContext();

var BUFF_SIZE = 16384;

var audioInput = null,
    microphone_stream = null,
    gain_node = null,
    script_processor_node = null,
    script_processor_fft_node = null,
    analyserNode = null;

function show_some_data(given_typed_array, num_row_to_display, label) {
  
  var size_buffer = given_typed_array.length;
  var index = 0;
  var max_index = num_row_to_display;
    
  for (; index < max_index && index < size_buffer; index += 1) {
    if (given_typed_array[index] > audioThreshold) {
      Speech.thresholdExceeded();
    }
  }
}

function start_microphone(stream) {
  gain_node = audioContext.createGain();
  gain_node.connect( audioContext.destination );
  console.log(audioContext)
  audioContext.destination = false;
  microphone_stream = audioContext.createMediaStreamSource(stream);
  
  script_processor_node = audioContext.createScriptProcessor(BUFF_SIZE, 1, 1);
  
  microphone_stream.connect(script_processor_node);
  
  script_processor_fft_node = audioContext.createScriptProcessor(2048, 1, 1);
  script_processor_fft_node.connect(gain_node);
  
  analyserNode = audioContext.createAnalyser();
  analyserNode.smoothingTimeConstant = 0;
  analyserNode.fftSize = 2048;
  
  microphone_stream.connect(analyserNode);
  
  analyserNode.connect(script_processor_fft_node);
  
  script_processor_fft_node.onaudioprocess = function() {
    
    // get the average for the first channel
    var array = new Uint8Array(analyserNode.frequencyBinCount);
    analyserNode.getByteFrequencyData(array);
    
    // draw the spectrogram
    if (microphone_stream.playbackState == microphone_stream.PLAYING_STATE) {
      
      show_some_data(array, 5, "from fft");
    }
  };
  // audioContext.close();
}