var selectedItem = false;

function fullscreenToggle() {  
  document.querySelector('.ytp-fullscreen-button').click()
}

function volumeUp() {
  // document.querySelector('.ytp-chrome-controls')
  if (document.getElementsByTagName('video')[0].volume < 1) {
    document.getElementsByTagName('video')[0].volume += 0.2
  }
}

function volumeDown() {
  if (document.getElementsByTagName('video')[0].volume < 0.2) {
    document.getElementsByTagName('video')[0].volume = 0
    document.getElementsByTagName('video')[0].volume -= 0.2
  } else {}
}

function mute() {
  document.querySelector('.ytp-mute-button').click()
}

function search(message) {
  document.querySelector('input#search').value = message.value
  document.querySelector('button[aria-label="Search"]').click()
}

function play() {
  document.querySelector('.ytp-play-button').click()
}

function changeView() {
  document.querySelector('.ytp-size-button').click()
}

function select() {
  if (selectedItem) {
    selectedItem.querySelector('a').click()
    selectedItem = false
    return;
  }
  if (location.href.indexOf("watch?v=") > -1) {
    selectedItem = document.querySelectorAll('.ytd-watch-next-secondary-results-renderer')[2]
  } else {
    selectedItem = document.querySelectorAll('.ytd-item-section-renderer')[3]    
  }
  selectedItem.className += " focused-speech-item"
  selectedItem.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"})    
}

function next() {
  if (selectedItem.nextElementSibling) {
    selectedItem.className = selectedItem.className.replace("focused-speech-item", "")
    selectedItem = selectedItem.nextElementSibling
    selectedItem.className += " focused-speech-item"
    selectedItem.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"})    
  }
}

function previous() {
  if (selectedItem.previousElementSibling) {
    selectedItem.className = selectedItem.className.replace("focused-speech-item", "")
    selectedItem = selectedItem.previousElementSibling
    selectedItem.className += " focused-speech-item"
    selectedItem.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"})    
  }
}

function back() {
  window.history.back()
}

const commands = {
  fullscreen: fullscreenToggle,
  volumeup: volumeUp,
  volumedown: volumeDown,
  mute: mute,
  search: search,
  play: play,
  stop: play,
  default: changeView,
  select: select,  
  next: next,  
  previous: previous,  
  back: back,
}

function runCommand(message, sender, sendResponse) {
  console.log(message)
  commands[message.command](message)  
}
chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  runCommand(message, sender, sendResponse)
});