function runCommand(message, sendResponse) {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var tab = tabs[0]
    message.command = message.command.replace(' ', '');
    chrome.tabs.sendMessage(tab.id, message)
      
  });
}

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  runCommand(message, sendResponse)
});