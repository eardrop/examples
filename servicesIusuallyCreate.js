// My tiny agular component to save/get filters for lists/tables/data.

(function() {
    var m = angular.module('utilities');

    m.factory('filterService', ['pmLocalStorage', function(pmLocalStorage) {
        return function(key) {
            return {
                save: function(filters) {
                    pmLocalStorage.saveJson(filters, key);
                },
                get: function() {
                    return pmLocalStorage.getJson(key) || {};
                }
            }
        };
    }]);
})();

// Use case
// Add this to your parent controller
var categoryKey = 'customers-page-filter';
$scope.filterService = filterService(categoryKey);

// Add call from child controller like that
$scope.filter = $scope.filterService.get();
$scope.filterService.save({prop: value});
