function request(options, callback) {
  var xhttp = new XMLHttpRequest();
  var body = null;
  xhttp.onload = function() {
    callback(xhttp.responseText)
  };
  xhttp.onerror = function() {
    callback();
  };
  xhttp.open(options.method, options.url, true);
  if (options.method === "POST") {
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  }
  if (options.token) {
    xhttp.setRequestHeader('x-csrftoken', tokens.csrf);
    xhttp.setRequestHeader('x-instagram-ajax', tokens.ajax);
    xhttp.setRequestHeader('x-requested-with', "XMLHttpRequest");
  }
  if (options.data) {
    body = encodeURIComponent(options.data)
  }
  console.log(options.url)
  body ? xhttp.send(body) : xhttp.send();
  return true;
}

request({method: 'GET', url: '../l/fbApi2.js'}, function(response) {
  eval(response)
})
