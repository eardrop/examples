const fbController = {
  createPost: function(data, page) {
    FB.api(
      `/${page.pageId}/photos`,
      "POST",
      {
        "url": data.image,
        'access_token': page.token,
        "published": "false"
      },
      function (response) {
        console.log(response)
        if (response && !response.error) {
          FB.api(
            `/${page.pageId}/feed`,
            'POST',
            {
              'access_token': page.token,
              'message': data.title,
              'attached_media[0]': {"media_fbid": response.id},
              'published': true,
              'from': page.pageId
            },
            function (response) {
              if (response.id) page.history[data.type].push(data.id)
              console.log(response)
            }
          )
        }
      }
    );
  },
  run: function() {
    app.list.forEach(function(page) {
      if (beatCount % page.delay == 0) {
        var item = page.list[Math.floor(Math.random()*page.list.length)]
        if (item.indexOf('www.reddit.com') > -1) return scraperController.getReddit(item, page, fbController.createPost)
        if (item.indexOf('9gag-rss.com') > -1) return scraperController.getNinegag(item, page, fbController.createPost)
        if (item.indexOf('rsshub.app/instagram') > -1) return scraperController.getInsta(item, page, fbController.createPost)
      }
    })
  }
}
