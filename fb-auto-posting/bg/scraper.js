var parser = new DOMParser()

var scraperController = {
  getReddit: function(item, page, cb) {
    request({method: 'GET', url: item + '/.rss'}, function(response) {
      var xmlDoc = parser.parseFromString(response, "text/xml")
      var items = xmlDoc.querySelectorAll('entry')
      var list = []
      items.forEach(function(item) {
        if (page.history.reddit.indexOf(item.querySelector('id').innerHTML) > -1) return
        list.push(item)
      })
      function testItem() {
        var item = list[Math.floor(Math.random()*list.length)]
        var itemContent = item.querySelector('content').innerHTML
        if (!itemContent.split('alt="')[1]) return testItem() 
        var title = page.tagsOnly ? generatorController.getHashtags(page.tags) : itemContent.split('alt="')[1].split('" title')[0]
        cb({
          image: itemContent.split('gt;&lt;a href="')[1].split('"')[0],
          title: title,
          type: 'reddit',
          id: item.querySelector('id').innerHTML
        }, page)
      }
      testItem()
    })
  },
  getNinegag: function(item, page, cb) {
    request({method: 'GET', url: item}, function(response) {
      var xmlDoc = parser.parseFromString(response, "text/xml")
      var items = xmlDoc.querySelectorAll('item')
      var list = []
      items.forEach(function(item) {
        if (page.history.ninegag.indexOf(item.querySelector('guid').innerHTML) > -1) return
        list.push(item)
      })
      function testItem() {
        var item = list[Math.floor(Math.random()*list.length)]
        var itemContent = item.querySelector('description').innerHTML
        if (!itemContent.split('img src="')[1]) return testItem()
        cb({
          image: itemContent.split('img src="')[1].split('"')[0],
          title: item.querySelector('title').innerHTML,
          type: 'ninegag',
          id: item.querySelector('guid').innerHTML
        }, page)
      }
      testItem()
    })
  },
  getInsta: function(item, page, cb) {
    request({method: 'GET', url: item}, function(response) {
      var xmlDoc = parser.parseFromString(response, "text/xml")
      var items = xmlDoc.querySelectorAll('item')
      var list = []
      items.forEach(function(item) {
        if (page.history.insta.indexOf(item.querySelector('guid').innerHTML) > -1) return
        list.push(item)
      })
      function testItem() {
        var item = list[Math.floor(Math.random()*list.length)]
        var itemContent = item.querySelector('description').innerHTML
        if (!itemContent.split('ferrer" src="')[1]) return testItem()
        cb({
          image: itemContent.split('ferrer" src="')[1].split('"')[0],
          title: generatorController.getHashtags(page.tags),
          type: 'insta',
          id: item.querySelector('guid').innerHTML
        }, page)
      }
      testItem()
    })
  },
}
