// Example of how I managed to put a file from chrome extension inside an input without a user flow
function createFile(blob, name) {
  var file = new File(
    [blob],
    name,
    {
      type: 'png',
      lastModified: new Date()
    }
  )
  return file
}
function addFileList(input, file_paths) {
  file_paths = [file_paths]
  const file_list = file_paths.map(fp => createFile(fp.blob, fp.name))
  file_list.__proto__ = Object.create(FileList.prototype)
  Object.defineProperty(input, 'files', {
    value: file_list,
    writeable: false,
  })
  return input
}
addFileList(INSERT_YOUR_INPUT_ELEMENT_HERE, {blob: <INSERT_YOUR_BLOB_HERE>, name: 'INSERT_YOUR_NAME_HERE'})
// And that's it. Only ~20 lines of code is needed to overcome some part of chrome input security measures.